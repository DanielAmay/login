/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.appDis.services;

import ec.edu.ups.appDis.business.GestionBancariaON;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author Daniel AMay
 */



@WebService
public class ServicioSOAP {

    @Inject
    private GestionBancariaON on;

    @WebMethod
    public String saludar(String nombre) {
        return "Hola " + nombre;
    }

     @WebMethod
    public String depositar(String idCuenta, double cantidad) {
        on.depositar(idCuenta, cantidad);
        System.out.println("depositando...");
        return "Depositado";
    }

     @WebMethod
    public String retiar(String idCuenta, double cantidad) {

        on.retirar(idCuenta, cantidad);
        System.out.println("retirando...");
        return "retirado";
    }
    
    @WebMethod
    public String transferir(String idCuentaOirgen, String idCuentaDestino,double cantidad){
    
    on.transaccion(idCuentaOirgen, idCuentaDestino, cantidad);
        return "Dinero Transferido";
    }
                                                                                                                                                                                                  
}
